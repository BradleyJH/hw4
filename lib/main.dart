/***********************
 * Bradley Harrison
 * CS481
 * HW 4
 * Due 10/1/2020
 **********************/
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Animation HW4',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Animation Test'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller =
        AnimationController
          (duration: const Duration(milliseconds: 5000), vsync: this,);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            RotationTransition(
              turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Icon(Icons.android, size: 100,),
              )
            ),
            RotationTransition(
                turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Icon(Icons.desktop_windows, size: 100,),
                )
            ),
            RotationTransition(
                turns: Tween(begin: 0.0, end: 1.0).animate(_controller),
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: Icon(Icons.desktop_mac, size: 100,),
                )
            ),
            SizeTransition(
               sizeFactor : Tween(begin: 0.0, end: 1.0).animate(_controller),
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Icon(Icons.ac_unit, size: 100,),
                )
            ),
            FlatButton(
              child: Text("Start Animation"),
              onPressed: () => _controller.forward(),
            ),
            FlatButton(
              child: Text("Reset Animation"),
              onPressed: () => _controller.reset(),
            ),
          ],
        ),
      ),
    );
  }
}